FROM scratch
ENV PATH="/:${PATH}"
COPY sidecar-injector /
ENTRYPOINT ["/sidecar-injector"]
